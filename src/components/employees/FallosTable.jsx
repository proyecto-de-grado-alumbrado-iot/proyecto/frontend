
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { useDispatch, useSelector } from 'react-redux';
import NotificationImportantIcon from '@material-ui/icons/NotificationImportant';
import LibraryAddCheckRoundedIcon from '@material-ui/icons/LibraryAddCheckRounded';
import moment from 'moment';
import { startFixFailure, startGetCrewInfo } from '../../actions/crewsActions';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));


export const  FallosTable=() =>{
  const dispatch = useDispatch()

  const {infoCrew} = useSelector(state => state.crews)
    const handleClick = (index) =>{
if(infoCrew){

  dispatch(startFixFailure(infoCrew[0]?.Pending[index].ifFailure))
  dispatch(startGetCrewInfo(infoCrew[0]?.Crew))
}
        
    }
    

  const classes = useStyles();

  return (
<div className={classes.demo}>
<List >
  {
  (infoCrew) && (infoCrew[0]?.Pending?.length >0)? 
  infoCrew[0]?.Pending.map((item, index )=>{
   return <ListItem key={index}>
      <ListItemAvatar>
        <Avatar>
          <NotificationImportantIcon />
        </Avatar>

      </ListItemAvatar>
      <ListItemText
        primary={item.NameDevice}
        secondary={item.typeFault}
        
      />
       <ListItemText
        primary={item.idAddress}
        secondary={ moment( item.TimeDetect).format('LLL')}
        
      />
      <ListItemSecondaryAction>
        <div className="crew__check-icon">

        <IconButton edge="end" aria-label="delete" onClick={()=>handleClick(index)}>
          <LibraryAddCheckRoundedIcon />
        </IconButton>
      </div>
      </ListItemSecondaryAction>

    </ListItem>
}): <h4>No hay fallos por arreglar para esta cuadrilla</h4>
  }
</List>
</div>)
}