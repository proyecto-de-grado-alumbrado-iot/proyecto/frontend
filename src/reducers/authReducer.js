import { authType } from "../types/authTypes";


const initialState = { //todo reducer requiere un estado inicial, importante!
  checking: true,
    token: null,
    user: null,
  
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case authType.authLogin:
      return { ...state, checking: false, ...action.payload };
    case authType.authCheckingFinish:
      return {
        ...state,
        checking: false,
      };
    case authType.authLogout:
      return {
        checking: false,
      };
    default:
      return state;
  }
};
