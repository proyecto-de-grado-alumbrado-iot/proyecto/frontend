import { Paper } from '@material-ui/core'
import React from 'react'
import { UserStepper } from './UserStepper'

export const UserManager = () => {
    return (
        <div className="user__main-content">
            <Paper>
            <UserStepper />
            </Paper>
        </div>
    )
}
