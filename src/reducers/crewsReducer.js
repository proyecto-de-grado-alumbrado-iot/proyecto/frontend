import { crewsTypes } from "../types/crewTypes";

const initialState = {
    crews: [],
    infoCrew: [],
   

}

export const crewsReducer = (state = initialState, action) => {
    switch (action.type) {
        case crewsTypes.loadCrews:

            return {
                ...state,
                crews: action.payload
            }

        case crewsTypes.loadFailsAndEmployees:

            return {
                ...state,
                infoCrew: action.payload
            }
    


        default:
            return state;
    }
}