import ReactApexChart from "react-apexcharts";
import React from 'react'
import moment from "moment";
import { useSelector } from "react-redux";



export const Linechart2 = () => {
  const { CVPTotal } = useSelector(state => state.dashboard);
  // const time = CVPTotal.map(item => moment(new Date(item.fecha)).format('X'))
  const time = CVPTotal.map(item => item.fecha)
  // console.log(time[0])
  // const time=[1,2,3,4]
  const corriente = CVPTotal.map(item => item.corriente);
  const voltaje = CVPTotal.map(item => item.voltaje);
  const potencia = CVPTotal.map(item => item.potencia);
  function generateDayWiseTimeSeries(time, serie) {


    var i = 0;
    var series = [];
    while (i < time.length) {
      var y = Math.round(serie[i] * 100) / 100;
      var myDate = time[i];
      myDate = myDate.split("-");
      var newDate = new Date(myDate[0], myDate[1] - 1, myDate[2]);
      series.push([newDate.getTime(), y]);
      i++;
    }
    return series
  }
  const state = {

    series: [{
      name: "Corriente",

      data: generateDayWiseTimeSeries(time, corriente)
    }],

    options: {
      title: {
        text: 'Corriente',

        style: {
          fontSize: '14px',
          fontWeight: 'bold',
          fontFamily: 'Open Sans',
          color: '#8f5bd9'
        },
      },
      colors: ['#0cbce4'],

      markers: {
        size: 5,
      },
      chart: {
        id: 'fb',
        // group: 'social',
        type: 'line',
        height: 160
      },
      yaxis: {
        labels: {
          minWidth: 40
        }
      },
      xaxis: {
        type: 'datetime'
      }
    },

    seriesLine2: [{
      name: "Voltaje",
      data: generateDayWiseTimeSeries(time, voltaje)
    }],
    optionsLine2: {
      title: {
        text: 'Voltaje',

        style: {
          fontSize: '14px',
          fontWeight: 'bold',
          fontFamily: 'Open Sans',
          color: '#8f5bd9'
        },
      },
      colors: ['#4c3073'],

      markers: {
        size: 5,
      },
      chart: {
        id: 'tw',
        // group: 'social',
        type: 'line',
        height: 160
      },
      yaxis: {
        labels: {
          minWidth: 40
        }
      },
      xaxis: {
        type: 'datetime'
      }
    },

    seriesArea: [{
      name: "Potencia",

      data: generateDayWiseTimeSeries(time, potencia)
    }],
    optionsArea: {
      title: {
        text: 'Potencia',

        style: {
          fontSize: '14px',
          fontWeight: 'bold',
          fontFamily: 'Open Sans',
          color: '#8f5bd9'
        },
      },
      colors: ['#f2522e'],

      markers: {
        size: 5,
      },
      chart: {
        id: 'yt',
        // group: 'social',
        type: 'line',
        height: 160
      },
      yaxis: {
        labels: {
          minWidth: 40
        }
      },
      xaxis: {
        type: 'datetime'
      }
    },


  };
  return (
    <div id="wrapper">
      <div id="chart-line">
        <ReactApexChart options={state.options} series={state.series} type="line" height={160} />
      </div>
      <div id="chart-line2">
        <ReactApexChart options={state.optionsLine2} series={state.seriesLine2} type="line" height={160} />
      </div>
      <div id="chart-area">
        <ReactApexChart options={state.optionsArea} series={state.seriesArea} type="line" height={160} />
      </div>
    </div>
  )
}

