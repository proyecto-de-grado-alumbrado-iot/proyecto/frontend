

import { FormControl, Grid, InputLabel, MenuItem, Select, Autocomplete } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import { CssTextField } from '../ui/InputOutlined';
import validator from 'validator'
import { startAddDevices, startGetCrew } from '../../actions/devicesActions';
import { startLoadingCommunas } from '../../actions/globalActions';
import { startAddUser } from '../../actions/userActions';


export const UserForm = () => {


    const [valid, setValidator] = useState({
        email1: '',
        nombre: '',
        password1: '',
        rol: '',

    })
    // const dispatch = useDispatch();

    const [formValues, handleInputChange] = useForm({
        email1: '',
        nombre: '',
        password1: '',
        rol: '',

    })

    const {
        email1,
        nombre,
        password1,
        rol,
    } = formValues;


    const handleValidator = (e, type) => {
        handleInputChange(e);
        const value = e.target.value;
        switch (type) {
            case "text":
                if (value.length < 1) {
                    setValidator({
                        ...valid,
                        [e.target.name]: '* El campo es obligatorio'
                    })
                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null
                    })
                }
                break;
            case "password":
                if (value.length < 6) {
                    setValidator({
                        ...valid,
                        [e.target.name]: '* Una contraseña debe tener al menos 6 caracteres'
                    })
                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null
                    })
                }
                break;
            case "email":
                if (!validator.isEmail(value)) {
                    setValidator({
                        ...valid,
                        [e.target.name]: '* El email no es valido'
                    })

                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null
                    })
                }
                break;
            default:
                if (value.length === 0) {

                    setValidator({
                        ...valid,
                        [e.target.name]: '* Campo obligatorio'

                    })
                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null

                    })
                }
                break;
        }
    }
    const dispatch = useDispatch()
    const handleSubmit = (e) => {


        e.preventDefault();
        if (isFormValid()) {

            dispatch(startAddUser({

                "email-user": email1,
                "password-user": password1,
                "name-user": nombre,
                "id-rol": rol,

            }));

        }
    }

    const isFormValid = () => {

// startAddUser
        if (!valid.email1 && !valid.nombre && !valid.password1 && !valid.rol && email1.length >0 &&
        nombre.length > 0  && password1.length >0 && !!rol   ) {
            return true
        } else {
            return false
        }

    }
    return (
        <form className="user__form" key="formCreateDevices" onSubmit={handleSubmit}>

            <Grid container spacing={2} justifyItems="center"
                justifySelf="center" alignSelf="center"
                alignItems="center" justifyContent="center" >

                <Grid item sm={6} xl={6} md={6} xs={12} >
                    <CssTextField

                        name="nombre"
                        value={nombre}
                        onChange={(e) => handleValidator(e, 'text')}
                        placeholder="INA219-1"
                        type="text"
                        label="Nombre"
                        variant='outlined'
                        error={!!valid.nombre}
                        helperText={(valid.nombre ? valid.nombre : '')}
                        autoSave="off"
                        size="small"
                        autoComplete='off'

                    />
                </Grid>
                <Grid item sm={6} xl={6} md={6} xs={12} >
                    <CssTextField
                        autoSave="off"
                        autoCorrect="off"

                        name="email1"
                        value={email1}
                        onChange={(e) => handleValidator(e, 'email')}
                        placeholder="Example@mail.com"
                        type="text"
                        label="Email"
                        variant='outlined'
                        error={!!valid.email1}
                        helperText={(valid.email1 ? valid.email1 : '')}

                        size="small"
                        autoComplete='off'

                    />
                </Grid>
                <Grid item sm={6} xl={6} md={6} xs={12} >
                    <CssTextField
                        autoSave="off"
                        autoCorrect="off"
                        name="password1"
                        value={password1}
                        onChange={(e) => handleValidator(e, 'password')}
                        placeholder="Example@mail.com"
                        type="password"
                        label="Password"
                        variant='outlined'
                        error={!!valid.password1}
                        helperText={(valid.password1 ? valid.password1 : '')}

                        size="small"
                        autoComplete='off'


                    />
                </Grid>

                <Grid item sm={6} xl={6} md={6} xs={12} >
                    <FormControl fullWidth={true} size="small" variant='outlined'>
                        <InputLabel id="demo-simple-select-label">Rol</InputLabel>
                        <Select
                            style={{
                                width: "90%"
                            }}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            name="rol"
                            value={rol}
                            onChange={(e) => handleValidator(e, 'text')}

                            size="small"
                            autoComplete='off'
                            label="Rol"



                        >
                            <MenuItem value={2}>Administrador</MenuItem>
                            <MenuItem value={3}>Empleado</MenuItem>
                            <MenuItem value={6}>Lider de cuadrilla</MenuItem>
                        </Select>
                    </ FormControl>

                </Grid>






            </Grid>
            <Grid item container
                direction="row"
                justify="center"
                alignItems="center" xs={12} justifyItems="center" alignContent="center" alignSelf="center" justifySelf="center">

                <button className="auth__button" type="submit" disabled={!isFormValid()} >
                    Registrar usuario
            </button>

            </Grid>

        </form>
    )
}
