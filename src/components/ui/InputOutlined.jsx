import { lighten, TextField, withStyles } from "@material-ui/core";


// export const InputOutlined = ({keyUnique, label = "", colorHover = '#8f5bd9', onChanged, value = "", type = 'text', caso, name }) => {
   export const CssTextField =withStyles({

        root: {
            fontFamily: 'Open Sans',
            margin: '0px',
            color: '#3c3c3c',
            height: '30px',
            width: '80%',
            marginBottom:"15px",
            '&:hover .MuiOutlinedInput-root  .MuiOutlinedInput-notchedOutline': {
                borderColor: `#8f5bd9`,

            },
            "&:hover .MuiInputLabel-outlined": {
                color: `#8f5bd9`,
            },

            "& .MuiInputLabel-outlined.Mui-focused": {
                color: lighten(`#8f5bd9`, 0.2)
            },
            "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
                borderColor: lighten(`#8f5bd9`, 0.2)
            }
            // "&:hover ":{
            //     borderColor:'#4c3073'
            // }

        },

    })(TextField);
    // switch (caso) {
    //     case 1:
    //         return (

    //             <CssTextField
    //             key={keyUnique}
    //                 type={type}
    //                 label={label}
    //                 name={name}

    //                 variant='outlined'
    //                 onChange={onChanged}
    //                 size="small"
    //                 value={value}
    //                 autoComplete='off'
    //                 InputProps={{
    //                     endAdornment: (
    //                         <InputAdornment>

    //                             {                 < AlternateEmailIcon />
    //                             }              </InputAdornment>
    //                     )
    //                 }}
    //             />

    //         )

    //     case 2:
    //         return (

    //             <CssTextField
    //             key={keyUnique}

    //                 type={type}
    //                 label={label}
    //                 variant='outlined'
    //                 onChange={onChanged}
    //                 value={value}
    //                 name={name}

    //                 size="small"
    //                 autoComplete='off'
    //                 InputProps={{
    //                     endAdornment: (
    //                         <InputAdornment>

    //                             {                 < VpnKeyIcon />
    //                             }              </InputAdornment>
    //                     )
    //                 }}
    //             />

    //         )
    //     default:
    //         return (<CssTextField
    //             key={keyUnique}

    //             type={type}
    //             id="outlined-basic"
    //             label={label}
    //             name={name}
    //             value={value}

    //             variant='outlined'
    //             onChange={onChanged}
    //             size="small"
    //             autoComplete='off'

    //         />

    //         )
    // }

// }

