import { Autocomplete, Paper } from '@material-ui/core'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { startGetCrewInfo, startGetCrews } from '../../actions/crewsActions'
import { startLoadingCommunas } from '../../actions/globalActions'
import { useForm } from '../../hooks/useForm'
import { CssTextField } from '../ui/InputOutlined'
import { TiGroup } from 'react-icons/ti';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { FallosTable } from './FallosTable'

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));
export const Employees = () => {
    const {user} = useSelector(state => state.auth)
    const rol = user['id-rol'];
    const idcrew = user['idcrew']

    const classes = useStyles();
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };
    const dispatch = useDispatch()
    const [value, handleForm] = useForm({
        comuna: null
    })
    const { communeList } = useSelector(state => state.global);
    const { crews } = useSelector(state => state.crews);

    
    useEffect(() => {
        dispatch(startLoadingCommunas())


    }, [dispatch])

    useEffect(() => {
        dispatch(startGetCrews(value.comuna))


    }, [dispatch, value])

    useEffect(() => {
        if (crews && crews?.length > selectedIndex)
            dispatch(startGetCrewInfo(crews[selectedIndex].idCrew))

    }, [dispatch, crews, selectedIndex])
    return (
        <div className="crew__main-content">
            <div className="crew__list-crew">
                <Paper>
                    <Autocomplete hidden={rol===4 }
                        id="combo-box-demo"
                        loading={communeList.length === 0}
                        autoSelect={true}
                        options={communeList}
                        getOptionLabel={(option) => option['name-commune']}
                        style={{ width: "100%" }}
                        isOptionEqualToValue={(option, value) => option['id-commune'] === value['id-commune']}
                        onChange={(event, value) =>
                            handleForm({
                                target: {
                                    value: value ? value['id-commune'] : null,
                                    name: 'comuna'
                                }
                            },
                            )}

                        // getoptionselected=


                        renderInput={(params) => <CssTextField {...params}
                            name="crew"
                            value={value.comuna}


                            type="text"
                            label="Comuna"
                            variant='outlined'

                            size="small"
                            autoComplete='off'

                        />}
                    />
                    <div className={classes.root}>
                        <List component="nav" aria-label="main mailbox folders">
                            {

                                crews.map((item, index) =>
                                (<div hidden={rol===4 && `${idcrew}`!== `${item.idCrew}`} key={index}> <ListItem

                                    key={index}
                                    button
                                    selected={selectedIndex === index}
                                    onClick={(event) => handleListItemClick(event, index)}
                                >
                                    <ListItemIcon  >
                                        <TiGroup color="primary" />
                                    </ListItemIcon>
                                    <ListItemText primary={item.Crew} secondary={item.Commune} />

                                </ListItem>

                                    <Divider key={index + 'd'} />
                                </div>)
                                )
                            }



                        </List>
                    </div>


                </Paper>
            </div>

            {(crews && crews?.length > selectedIndex) &&
                <div key={selectedIndex} hidden={rol===4 && `${idcrew}`!== `${crews[selectedIndex].idCrew}`} className="crew__info animate__animated animate__zoomIn">
                    <Paper>
                        <div className="crew__card ">
                            <h2><TiGroup color="primary" /> {crews[selectedIndex].Crew}</h2>
                            <h3>Miembros del Equipo</h3>


                            {crews[selectedIndex].employee ?
                                <ul >
                                    {crews[selectedIndex].employee?.map(item => <li key={item.id} >
                                     
                                        <div className="crew__item-list" >
                                            <div >
                                                <span>{item.name + ' ' + item.lastname}</span><br />
                                                <span>{item.rol}</span>
                                            </div>
                                            <div >
                                                {item.phone}
                                            </div>
                                        </div>
                                        <Divider  />
                                    </li>)
                                     
}
                                </ul>
                                : <h4>No hay miembros en esta cuadrillas</h4>
                            }
                        </div>



                        <h3>Fallos pendientes</h3>
                        <div className="crew__list-pending">
                            <FallosTable />
                        </div>
                    </Paper>
                </div>
            }
        </div>
    )
}
