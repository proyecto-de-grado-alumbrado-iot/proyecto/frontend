
import React from 'react'
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

export const BarchartGrouped = () => {
  const { errorTypes } = useSelector(state => state.dashboard);
  let array1 = [];
  let array2 = [];
  let array3 = [];
  let array4 = [];
  let array5 = [];
  let time = [];
  errorTypes.forEach(item => {
    let index = time.indexOf(item.Date);
    if (index === -1) {

      time.push(item.Date)
      index = time.length - 1;
      array1.push(0)
      array2.push(0)
      array3.push(0)
      array4.push(0)
      array5.push(0)
    } 
    switch (item['type-output']) {

      case 1:
        array1[index]=item.Total

        break;
      case 2:
        array2[index]=item.Total

        break;
      case 3:
        array3[index]=item.Total

        break;
      case 4:
        array4[index]=item.Total

        break;
      case 5:
        array5[index]=item.Total

        break;
      default:
        
        break
    }
  }
  )
  const state = {

    series: [{
      name: 'Normal',
      data: array1
    }, {
      name: 'HIGH-USE',
      data: array2
    }, {
      name: 'LOW-USE',
      data: array3
    }, {
      name: 'DAY-ON',
      data: array4
    }, {
      name: 'NIGHT-OFF',
      data: array5
    }],
    options: {
      title:{
        text:"Clasificación de errores por día",},
      chart: {
        type: 'bar',
        height: 430
      },
      plotOptions: {
        bar: {
          horizontal: true,
          dataLabels: {
            position: 'top',
          },
        }
      },
      dataLabels: {
        enabled: false,
        offsetX: -6,



        style: {
          fontSize: '12px',
          colors: ['#000000']
        }
      },
      // stroke: {
      //   show: true,
      //   width: 3,
      //   colors: ['#fff']
      // },
      tooltip: {
        shared: true,
        intersect: false
      },
      xaxis: {
        categories: time,
      },
    },


  };
  return (
    <div id="chart">
      <ReactApexChart options={state.options} series={state.series} type="bar" height={430} />
    </div>
  )
}



