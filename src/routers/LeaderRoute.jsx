

import { AppBar } from "../components/ui/AppBar";
import React, { useEffect, useState } from "react";
import {
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
import { Devices } from "../components/devices/Devices";
import { Employees } from "../components/employees/Employees";
import { Sidebar } from "../components/ui/Sidebar";
import { useSelector } from "react-redux";
import { LoadingPage } from "../components/ui/LoadingPage";


export const LeaderRoute = () => {
    const { hide } = useSelector(state => state.ui);


    const [animation, setAnimation] = useState('');
    
    useEffect(() => {
        if(hide){
            setAnimation('animate__animated animate__slideOutLeft')
        }else{
            if(animation.length !== 0 )
            setAnimation('animate__animated animate__slideInLeft')

            
        }
     
    }, [hide, animation])
    const[value, setValues]=useState(null)
    useEffect(() => {
        setValues (localStorage.getItem('token')) ;

    }, [])
    return (
        <>
            <div className="home__main-content" >
      <div className={hide?'home__sidebar-close': 'home__sidebar-open'} >

                    <Sidebar hide={hide}/>
      </div>
                <main className={!hide?'home__main-margin':''}>
                    <AppBar />
                    <div className="home__screen-content">
                    {(value)?
                    <Switch>
                        

                        <Route path="/devices" component={Devices} />
                        <Route path="/employees" component={Employees} />

                        

                      
                        <Redirect to='/devices' />


                    </Switch>:<LoadingPage /> 
                    }
                    </div>
                </main>
            </div>
        </>
    )
}
