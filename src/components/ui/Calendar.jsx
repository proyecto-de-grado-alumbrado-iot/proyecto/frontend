import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import DateRangePicker from '@material-ui/lab/DateRangePicker';
import Box from '@material-ui/core/Box';

export const CalendarsDateRangePicker=({setValue, value})=> {

  return (
    
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Box>
        <DateRangePicker
          calendars={1}
          value={value}
          onChange={(newValue) => {
            setValue(newValue);
          }}
          renderInput={(startProps, endProps) => (
            <React.Fragment>
              <Box sx={{ mx: 2 }}> desde </Box>

              <TextField {...startProps} label="Fecha inicial"/>
              <Box sx={{ mx: 2 }}> hasta </Box>
              <TextField {...endProps} label="Fecha final" />
            </React.Fragment>
          )}
        />
       
      </Box>
    </LocalizationProvider>
  );
}
