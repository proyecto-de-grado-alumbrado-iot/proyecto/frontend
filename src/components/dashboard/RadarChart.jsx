
import React from 'react'
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

export const RadarChart = () => {

  const {respTime} = useSelector(state => state.dashboard);

   const state = {
      
        series: [{
          name: 'Tiempo',
          data: respTime.map(item => item['performance']),
        }],
        options: {
          chart: {
            height: 350,
            type: 'radar',
          },
          dataLabels: {
            enabled: true
          },
          plotOptions: {
            radar: {
              size: 140,
              polygons: {
                strokeColors: '#e9e9e9',
                fill: {
                  colors: ['#f8f8f8', '#fff']
                }
              }
            }
          },
          title: {
            text: 'Tiempo de respuesta por cuadrilla'
          },
          colors: ['#FF4560'],
          markers: {
            size: 4,
            colors: ['#fff'],
            strokeColor: '#FF4560',
            strokeWidth: 2,
          },
          tooltip: {
            y: {
              formatter: function(val) {
                return `${val} s` // convertir a horas
              }
            }
          },
          xaxis: {
            categories: respTime.map(item => `Cuadrilla ${item['idCrew']}`),
        
          },
          yaxis: {
            tickAmount: 7,
            labels: {
              formatter: function(val, i) {
                if (i % 2 === 0) {
                  return val
                } else {
                  return ''
                }
              }
            }
          }
        },
      
      
      };
    return (
        <div id="chart">
        <ReactApexChart options={state.options} series={state.series} type="radar" height={350} />
        </div>
    )
}
