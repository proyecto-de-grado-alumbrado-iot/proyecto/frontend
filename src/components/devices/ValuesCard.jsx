import { Box, Grid, Paper } from '@material-ui/core'
import React, { useCallback, useEffect, useState } from 'react'


import { AreaChart,AreaChart2,AreaChart3 } from './AreaChart';
import { ValuesItem } from './ValuesItem';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { addValuesToDevices, resetDevices } from '../../actions/devicesActions';
import { mqttDisconnect, mqttfun, mqttSuscribe } from '../../actions/mqttActions';
import { process_msg } from '../../utils/readValuesMqtt';

export const ValuesCard = ({ id}) => {
    const history =useHistory();
    const [clientMqtt, setClient] =useState(null)
    const dispatch = useDispatch();
    history.listen((location, action) => {
        if (location.pathname !== '/devices' && clientMqtt) {
          mqttDisconnect(clientMqtt);
          dispatch(resetDevices())
    
    
        }
        // location is an object like window.location
      });

      const handleMessage=useCallback(async(topic,message)=>{
        console.log("1 .Mensaje recivido bajo topico: ", topic, message.toString());
        const newVal = await process_msg(topic, message);
            dispatch(addValuesToDevices( newVal));
        
   },[dispatch]);

 

    useEffect(() => {
        

           const client= mqttfun();
           setClient(client)
           
           mqttSuscribe(client);
           const callBack = async(topic,mqttMessage) => handleMessage(topic,mqttMessage);
           client.on('message', callBack)


        // return () => mqttDisconnect(client);
    }, [handleMessage])

    return (
        <Box width={0.9} p={5}>
            <Grid container spacing={2} >
                <Grid item xs={4} className="devices__values-paper">
                    <Paper elevation={3} >
                        <div>
                            <h3 className="devices__title-values">Corriente </h3>
                            <div className='devices__values'>
                              <ValuesItem tipo={0} id={id}/>
                            </div>
                            <AreaChart/>
                        </div>
                    </Paper>
                </Grid>

                <Grid item xs={4} className="devices__values-paper">
                    <Paper elevation={3} >
                        <div>

                            <h3 className="devices__title-values">Voltaje</h3>
                            <div className='devices__values'>
                              <ValuesItem tipo={1} id={id}/>

                                {/* <span > {voltage==='Off' ? voltage : `${voltage} mV`} </span> */}
                            </div>
                            <AreaChart2/>

                        </div>
                    </Paper>
                </Grid>

                <Grid item xs={4} className="devices__values-paper">
                    <Paper elevation={3} >
                        <div>

                            <h3 className="devices__title-values">Potencia</h3>
                            <div className='devices__values'>
                              <ValuesItem tipo={2} id={id}/>

                                {/* <span id="display_power"> {power==='Off'?power:`${power} mW`} </span> */}
                            </div>
                            <AreaChart3/>

                        </div>


                    </Paper>
                </Grid>
            </Grid>
        </Box>
    )
}
