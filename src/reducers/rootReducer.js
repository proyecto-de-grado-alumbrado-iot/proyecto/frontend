import { combineReducers } from "redux";
import { authReducer } from "./authReducer";
import { devicesReducer } from "./devicesReducer";
import { globalReducer } from "./globalReducer";
import { uiReducer } from "./uiReducer";
import { dashboardReducer } from "./dashboardReducer";
import { crewsReducer } from "./crewsReducer";
import { userReducer } from "./userReducer";

export const rootRecucer = combineReducers({
  auth: authReducer,
  ui:uiReducer,
  devices:devicesReducer,
  dashboard: dashboardReducer,
  global: globalReducer,
  crews: crewsReducer,
  
  user: userReducer
});
