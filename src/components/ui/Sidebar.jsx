import React from 'react'
import MemoryIcon from '@material-ui/icons/Memory';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import PeopleIcon from '@material-ui/icons/People';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../../actions/authActions';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
export const Sidebar = ({hide}) => {
const dispatch = useDispatch()
const {user} = useSelector(state => state.auth)



const handleClick = () => {
    dispatch(logout())
}
    return (
            <div className={hide?'home__sidebar home__sidebar-close':'home__sidebar home__sidebar-open'}>
                <div className="home__sidebar-navbar">
                    <div className="home__sidebar-avatar-border">
                        <img
                            alt="Avatar"
                            className="home__sidebar-avatar mt-5"
                            src="https://www.womentech.net/sites/default/files/inline-images/Anna%20Radulovski%2C%20WomenTech%20Network.png"
                        >

                        </img>
                    </div>
                    <h3 className="mt-5 ml-5 mr-5">Bienvenido, {user ? user['name-user'] : 'usuario'}</h3>
                    <p> <b>Rol: </b> {user?.rolName}</p>
                </div>
                <div className="home__sidebar-navbar-items"  >
               
                {user['id-rol']!==4 &&
                    <NavLink
                  
                        className="home__sidebar-item"
                        activeClassName="home__sidebar-item-active"
                        to="/dashboard">
                        <EqualizerIcon />
                        <p > Dashboard</p>
                    </NavLink>
}
                    <NavLink className="home__sidebar-item"
                        activeClassName="home__sidebar-item-active"
                        exact
                        to="/devices">
                        <MemoryIcon />
                        <p > Dispositivos</p>
                    </NavLink>
                    <NavLink className="home__sidebar-item"
                        activeClassName="home__sidebar-item-active"
                        exact
                        to="/employees">
                        <PeopleIcon />
                        <p > Cuadrillas y empleados</p>
                    </NavLink>
                    {user['id-rol']!==4 &&
                    <NavLink className="home__sidebar-item"
                        activeClassName="home__sidebar-item-active"
                        exact
                        to="/users">
                        <PersonAddIcon />
                        <p > Gestión de usuarios</p>
                    </NavLink>
}
                    <hr className="mt-5 mb-1 ml-5" />

                    <button className="btn home__logout" onClick={handleClick}>
                        <ExitToAppIcon />
                        <p > Cerrar sesión</p>
                    </button>

                    <div className="home__bottom">

                    </div>

                </div>
            </div >
            
        )
}
