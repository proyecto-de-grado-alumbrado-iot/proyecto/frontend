import { Box, Grid, IconButton, Paper } from '@material-ui/core'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { getReporte, startGetFailsxComunas, startLoadCPV, startLoadFaultFix, startLoadTopDevicesFails, startloadTypeError } from '../../actions/dashboardActions'
import { CalendarsDateRangePicker } from '../ui/Calendar'

import { BarchartGrouped } from './BarchartGrouped'
import { Linechart2 } from './Linechart2'
import { MapFails } from './MapFails'
import { MixedBarChart } from './MixedBarChart'
import { PieChart } from './PieChart'
import { RadarChart } from './RadarChart'
import DeleteIcon from '@material-ui/icons/Delete';
import GetAppIcon from '@material-ui/icons/GetApp';
export const Dashboard = () => {
    const dispatch = useDispatch();
    const [value, setValue] = React.useState([null, null]);


    useEffect(() => {
if((!value[0] && !value[1]) || (value[0] && value[1])){

    dispatch(startGetFailsxComunas(value[0], value[1]));
    dispatch(startLoadCPV(value[0], value[1]));
    dispatch(startLoadTopDevicesFails(10, value[0], value[1]));
    dispatch(startLoadFaultFix(value[0], value[1]));
    dispatch(startloadTypeError(value[0], value[1]));
}


    }, [dispatch, value]);

const handleReport=()=>{
    dispatch(getReporte(value[0], value[1]))
}
    return (
        <div className="dashboard__main-content">
            <Grid container spacing={3} justifyContent="center" alignContent="center">
                <Grid item xs={8} sm={8}  >
                    <Paper>
                        <Grid container spacing={3} justifyContent="space-evenly">
                            <Grid item xs={8}>
                                < CalendarsDateRangePicker setValue={setValue} value={value} />
                            </Grid>
                            <Grid item xs={1}>

                                <IconButton aria-label="delete" onClick={(e) => setValue([null, null])}>
                                    <DeleteIcon fontSize="large" />
                                </IconButton>


                            </Grid>




                        </Grid>
                    </Paper>

                </Grid>
                <Grid item xs={4} >
                    <Paper >
                        <div className="dashboard__generar-rep">
                        <h3>Generar reporte</h3>
                        <button className="auth__button" onClick={handleReport}><GetAppIcon/></button>
                        </div>
                    </Paper>

                </Grid>

                <Grid item xs={12}>
                    <Paper >
                        <Linechart2 />
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper >

                        <MapFails />

                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper  >
                        <Box height="480px">
                            <PieChart />
                        </Box>
                    </Paper>
                </Grid>
                <Grid item xs={5}>
                    <Paper ><RadarChart /></Paper>
                </Grid>
                <Grid item xs={7}>
                    <Paper ><MixedBarChart /></Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper ><BarchartGrouped /></Paper>
                </Grid>

            </Grid>
        </div>
    )
}
