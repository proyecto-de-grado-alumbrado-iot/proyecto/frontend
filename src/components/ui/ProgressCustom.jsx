import { Backdrop, CircularProgress, makeStyles } from '@material-ui/core'
import React from 'react'
import { shallowEqual, useSelector } from 'react-redux';

export const ProgressCustom = () => {
    const { loading } = useSelector(state => state.ui,shallowEqual)
    const useStyles = makeStyles((theme) => ({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            color: '#fff',
        },
    }));
    const classes = useStyles();
    return (
        <Backdrop className={classes.backdrop} open={loading} >
        <CircularProgress  />
    </Backdrop>
    )
}
