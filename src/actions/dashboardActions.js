import Swal from "sweetalert2";
import { dashboardTypes } from "../types/dashboardTypes";
import { httpToken } from "../utils/fetch";
import { startChecking } from "./authActions";
import saveAs from 'save-as'
import { finishLoading, startLoading } from "./uiActions";

export const startGetFailsxComunas = (initDate=null,
  endDate=null) => {
    return async (dispatch) => {
      try {
        dispatch(startLoading() )

        const body = (initDate && endDate )?{
          initDate,
          endDate
        }:{};
       
        const resp = await httpToken(`dashboard/fault`, body, 'POST');
        const data = await resp.json();
        dispatch(finishLoading() )

        if (data.ok) {
          dispatch(getFailsxComunas(data.body));
        } else {
  
          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
  
        }
      } catch (e) {
        dispatch(finishLoading() )

        Swal.fire('Error', `${e}`, 'error');
  
      }
  
    }

};

export const getReporte = (initDate=null,
  endDate=null) => {
    return async (dispatch) => {
      try {
        dispatch(startLoading() )

        const body = (initDate && endDate )?{
          initDate,
          endDate
        }:{};
        const resp = await httpToken(`dashboard/reportFault`, body, 'POST');
        const bodyDoc =await resp.blob();
      
        dispatch(finishLoading() )
        saveAs( bodyDoc,'reporte.xlsx')

       
      } catch (e) {
        dispatch(finishLoading() )

        Swal.fire('Error', `${e}`, 'error');
  
      }
  
    }

};

export const startLoadCPV = (initDate, endDate) => {
  return async (dispatch) => {
    try {
      dispatch(startLoading() )

      const body = (initDate && endDate )?{
        initDate,
        endDate
      }:{};

      const resp = await httpToken(`dashboard/infoDate`, body, 'POST');
      const data = await resp.json();
      dispatch(finishLoading() )

      if (data.ok) {
        dispatch(loadCVP(data.body));
      } else {

        Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');

      }
    } catch (e) {
      dispatch(finishLoading() )

      Swal.fire('Error', `${e}`, 'error');

    }

  }

};

export const startLoadTopDevicesFails = (limite=10,initDate,endDate) =>{
  // 
  return async (dispatch) => {
    try {
      dispatch(startLoading() )

      const body = {
        limite,
        
      };
      if(initDate) body.initDate=initDate;
      if(endDate) body.endDate=endDate;
      const resp = await httpToken(`dashboard/topDevice`, body, 'POST');
      const data = await resp.json();
      dispatch(finishLoading() )

      if (data.ok) {
        dispatch(loadTopFailsDevices(data.body));
      } else {

        Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');

      }
    } catch (e) {
      dispatch(finishLoading() )

      Swal.fire('Error', `${e}`, 'error');

    }

  }
}


export const startLoadFaultFix = (initDate,endDate) =>{
  // 
  return async (dispatch) => {
    try {
      dispatch(startLoading() )

      const body = (initDate &&
        endDate)?{
        initDate,
        endDate
      }:{};
      const resp = await httpToken(`dashboard/faultFix`,body, 'POST');
      const data = await resp.json();
      dispatch(finishLoading() )

      if (data.ok) {
        dispatch(loadRespTime(data.body));
      } else {
       

          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
        


      }
    } catch (e) {
      dispatch(finishLoading() )

      Swal.fire('Error', `${e}`, 'error');

    }

  }
}

export const startloadTypeError = (initDate,endDate) =>{
  // 
  return async (dispatch) => {
    try {
      dispatch(startLoading() )

      const body = (initDate &&
        endDate)?{
        initDate,
        endDate
      }:{};
      const resp = await httpToken(`dashboard/infoTypeOutput`,body, 'POST');
      const data = await resp.json();
      dispatch(finishLoading() )

      if (data.ok) {
        dispatch(loadTypeError(data.body));
      } else {
       

          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');

      }
    } catch (e) {
      dispatch(finishLoading() )
      Swal.fire('Error', `${e}`, 'error');

    }

  }
}

const loadRespTime = (respTime) => ({
  type: dashboardTypes.loadRespTime,
  payload: respTime
})
const loadCVP = (CVP)=>({
  type: dashboardTypes.loadCVPtotal,
  payload: CVP

})

const getFailsxComunas = (fails)=>({
    type: dashboardTypes.loadFailsComuna,
    payload: fails
  
  });

  const loadTopFailsDevices = (devices)=>({
    type: dashboardTypes.loadTopDevices,
    payload: devices
  })
  const loadTypeError = (errors)=>({
    type: dashboardTypes.loadTypeError,
    payload: errors
  })
  