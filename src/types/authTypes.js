export const authType = {
    authChecking: "[auth] Checking login state",
    authCheckingFinish: "[auth] Start login",
    authLogin: "[auth] Login",
    authStartRegister: "[auth] Start register",
    verifyToken: "[auth] Verify token",
    authLogout: "[auth] Logout",

    uiSetError: '[UI] Set Error',
    uiRemoveError: '[UI] Remove Error',
}