import React from 'react';
import { Tooltip, CircleMarker, TileLayer, MapContainer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { useSelector } from 'react-redux';
// let data = {
//     city: [
//         { "name": "NORTE", "coordinates": [7.153050361258138, -73.1319239147585], "population": 37843000 },
//         { "name": "NORORIENTAL", "coordinates": [7.150022580179862, -73.12765121796502], "population": 30539000 },
//         { "name": "SAN FRANCISCO", "coordinates": [7.151322386708248, -73.12556315538868], "population": 24998000 },
//         { "name": "OCCIDENTAL", "coordinates": [7.139253926832348, -73.12795525626204], "population": 23480000 },
//         { "name": "GARCÍA ROVIRA", "coordinates": [7.124831204684554, -73.13488156694298], "population": 23416000 },
//         { "name": "LA CONCORDIA", "coordinates": [7.108699637276404, -73.11866066930747], "population": 22123000 },
//         { "name": "LA CIUDADELA", "coordinates": [7.108828552814042, -73.12721008431801], "population": 21009000 },
//         { "name": "SUROCCIDENTE", "coordinates": [7.100500587778141, -73.1255323033135], "population": 17712000 },
//         { "name": "LA PEDREGOSA", "coordinates": [7.104514308825463, -73.10886107263983], "population": 17444000 },
//         { "name": "PROVENZA", "coordinates": [7.090898542525309, -73.11469962226793], "population": 16170000 },
//         { "name": "SUR", "coordinates": [7.085602946408884, -73.13870256625466], "population": 15669000 },
//         { "name": "CABECERA DEL LLANO", "coordinates": [7.12340166162325, -73.10964623090854], "population": 14998000 },
//         { "name": "ORIENTAL", "coordinates": [7.129080997222798, -73.11335982682377], "population": 14667000 },
//         { "name": "MORRORICO", "coordinates": [7.14164825438938, -73.10323391885247], "population": 13287000 },
//         { "name": "CENTRO", "coordinates": [7.125788644214406, -73.12762269204526], "population": 13287000 },
//         { "name": "LAGOS DEL CACIQUE", "coordinates": [7.1289284471630365, -73.09843753354708], "population": 13287000 },
//         { "name": "MUTIS", "coordinates": [7.111774893019282, -73.13145525540034], "population": 13287000 },
//     ],
 
// };
export const MapFails = () => {
    const {failsComuna} = useSelector(state => state.dashboard)
   const data = failsComuna
  const minLat= 8.13277777778;
  const maxLat=5.70944444444;
  const minLong= -74.5333333333;
  const maxLong= -72.433333333;
    var centerLat = 7.1170830333936435;
    var distanceLat = data.maxLat - data.minLat;
    var bufferLat = distanceLat * 0.05;
    var centerLong =  -73.11829989036693;
    var distanceLong = data.maxLong - data.minLong;
    var bufferLong = distanceLong * 0.15;
    return (
      <div>
        <h3 style={{ textAlign: "center" }}>Fallos por sector</h3>
        <MapContainer
        
          style={{ height: "480px", width: "100%" }}
          zoom={12}
          center={[centerLat, centerLong]}
          bounds={[
            [minLat - bufferLat, minLong - bufferLong],
            [maxLat + bufferLat, maxLong + bufferLong]
          ]}
        >
            <TileLayer
    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  />

{data.map((fail) => {
            return (
              <CircleMarker
                key={fail.id}
                center={[fail.lat, fail.lon]}
                // radius={20 * Math.log(fail.totalFallas + 1)}
                radius={20 * Math.log(fail.totalFallas*1.5)}
                fillOpacity={0.5}
                stroke={false}
                color="red"
              >
                  <Tooltip direction="right" offset={[-8, -2]} opacity={1}><h3>{` ${fail.totalFallas} Fallos`}</h3><p>{`${fail.nombre}`}</p></Tooltip>
              
              </CircleMarker>
              )
          })
          }
         
        </MapContainer>
      </div>
    );
}
