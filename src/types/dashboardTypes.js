export const dashboardTypes = {
    loadFailsComuna: "[dashboard] load fails x comuna",
    loadCVPtotal:  "[dashboard] load current, voltage and power device",
    loadTopDevices:  "[dashboard] load top devices",
    loadRespTime:  "[dashboard] load response time",
    loadTypeError:  "[dashboard] load type error",
    
}