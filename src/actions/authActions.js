// import { authType } from "../types/authTypes";

import Swal from "sweetalert2";
import { authType } from "../types/authTypes";
import { httpNoToken, httpToken } from "../utils/fetch";
import { finishLoading, startLoading } from "./uiActions";


export const startLogin = (email = "", password = "") => {
  return async (dispatch) => {

    const body = {
      email: email,
      password: password
    };
    try {
      dispatch(startLoading() )
      const resp = await httpNoToken("auth/signIn", body, "POST");
      const data = await resp.json();
      const idcrewRaw = await httpNoToken(`employees/${email}`);
      const idcrew = await idcrewRaw.json();

      
      if (resp.ok) {

        let rolName;

        switch (data.user['id-rol']) {
          case 1:
            rolName = 'Super Administrador'
            break;
          case 2:
            rolName = 'Administrador'
            break;
          case 4:
            rolName = 'Lider de escuadron'
            break;
          case 3:
            rolName = 'Empleado'
            break;
          default:
            rolName = 'Empleado'
            break;
        }

        let token = data.token;
        let user = {
          ...data.user,
          rolName,
          idcrew:idcrew?.body
        }

        dispatch(login({token, user}));
        localStorage.setItem('token',token);
        localStorage.setItem('user', JSON.stringify(user))
        dispatch(finishLoading() )

      } else {
        dispatch(finishLoading() )

        Swal.fire('Credenciales incorrectas', data.body, 'warning');

      }
    } catch (e) {
      dispatch(finishLoading() )

      if(e==='jwt expired'){
        Swal.fire('La sesión expiró', `Inicie sesión`, 'info');

      }else{

        Swal.fire('Error', `${e}`, 'error');
      }

    }

  }
};





const login = (user) => ({
  type: authType.authLogin,
  payload: user,
});

export const logout = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('user');
  return {
    type: authType.authLogout,
  }
};


export const startChecking = () => {
  return async (dispatch) => {

    const resp = await httpToken('auth/verifyToken');
    const body = await resp.json();
    if (body.ok) {

      const token = localStorage.getItem('token');
      const user = localStorage.getItem('user');



      dispatch(login({
        token: token,
        user: JSON.parse(user)
        

      }))
    } else {

      dispatch(logout())
      dispatch(checkingFinish())
    }

  }
}



const checkingFinish = () => ({ type: authType.authCheckingFinish });