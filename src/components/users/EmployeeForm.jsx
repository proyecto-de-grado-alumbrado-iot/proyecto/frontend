import { FormControl, Grid, InputLabel, MenuItem, Select,Autocomplete } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import { CssTextField } from '../ui/InputOutlined';
import validator from 'validator'
import { startAddDevices, startGetCrew } from '../../actions/devicesActions';
import { startLoadingCommunas } from '../../actions/globalActions';


	// `lastname-employee` 
	// `phone-employee`
	// `name-employee`
	// `id-user` 
	// `id-crew` 
	// `born-date-employee`
	// `title-employee` 
export const EmployeeForm = () => {

    const dispatch = useDispatch();
    useEffect(() => {

        dispatch(startLoadingCommunas());

    }, [dispatch]);

    

    const { communeList } = useSelector(state => state.global);
    const { crews } = useSelector(state => state.devices);

    const [valid, setValidator] = useState({
        lastname: '',
        nombre: '',
        phone: '',
        iduser: '',
        idcrew: '',
        borndate: '',
        title: '',
       
   
    })
    // const dispatch = useDispatch();

    const [formValues, handleInputChange] = useForm({
        lastname: '',
        nombre: '',
        phone: '',
        iduser: '',
        idcrew: '',
        borndate: '',
        title: '',
        comuna:'',


    })

    const {
        lastname,
        nombre,
        phone,
        iduser,
        idcrew,
        borndate,
        title,
        comuna
    } = formValues;
    
    useEffect(() => {

        dispatch(startGetCrew(comuna));

    }, [dispatch, comuna]);

    useEffect(() => {

        dispatch(startLoadingCommunas());

    }, [dispatch]);
    const handleValidator = (e, type) => {
        handleInputChange(e);
        const value = e.target.value;
        switch (type) {
            case "text":
                if (value?.length < 1) {
                    setValidator({
                        ...valid,
                        [e.target.name]: '* El campo es obligatorio'
                    })
                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null
                    })
                }
                break;
            case "email":
                if (!validator.isEmail(value)) {
                    setValidator({
                        ...valid,
                        [e.target.name]: '* El email no es valido'
                    })

                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null
                    })
                }
                break;
            default:
                if (value?.length === 0) {

                    setValidator({
                        ...valid,
                        [e.target.name]: '* Campo obligatorio'

                    })
                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null

                    })
                }
                break;
        }
    }

    const handleSubmit = (e) => {


        e.preventDefault();
        if (isFormValid()) {

            dispatch(startAddDevices({

      	'lastname-employee' :lastname,
	'phone-employee':phone,
	'name-employee':nombre,
	'id-user' :iduser,
	'id-crew' :idcrew,
	'born-date-employee':borndate,
	'title-employee':title,
               
            }));

        }
    }

    const isFormValid = () => {


        if (!valid.lastname && !valid.nombre && !valid.idcrew ) {
            return true
        } else {
            return false
        }

    }
    return (
        <form className="user__form" key="formCreateDevices" onSubmit={handleSubmit}>

            <Grid container spacing={2}
                alignItems="center" >

                <Grid item sm={6} xl={4} md={4} xs={12} >
                    <CssTextField

                        name="nombre"
                        value={nombre}
                        onChange={(e) => handleValidator(e, 'text')}
                        placeholder="Maria"
                        type="text"
                        label="Nombre"
                        variant='outlined'
                        error={!!valid.nombre}
                        helperText={(valid.nombre ? valid.nombre : '')}

                        size="small"
                        autoComplete='off'

                    />
                </Grid>
                <Grid item sm={6} xl={4} md={4} xs={12} >
                    <CssTextField

                        name="lastname"
                        value={lastname}
                        onChange={(e) => handleValidator(e, 'text')}
                        placeholder="Perez"
                        type="text"
                        label="Apellidos"
                        variant='outlined'
                        error={!!valid.lastname}
                        helperText={(valid.lastname ? valid.lastname : '')}

                        size="small"
                        autoComplete='off'

                    />
                </Grid>
                <Grid item sm={6} xl={4} md={4} xs={12} >
                    <CssTextField

                        name="phone"
                        value={phone}
                        onChange={(e) => handleValidator(e, 'number')}
                        placeholder="1238853"
                        type="number"
                        label="Telefono"
                        variant='outlined'
                        error={!!valid.phone}
                        helperText={(valid.phone ? valid.phone : '')}

                        size="small"
                        autoComplete='off'

                    />
                </Grid>
              

          
             
                <Grid item sm={6} xl={4} md={4} xs={12} >

                    <Autocomplete
                        id="combo-box-demo"
                        loading={communeList.length === 0}
                        autoSelect={true}
                        options={communeList}
                        getOptionLabel={(option) => option['name-commune']}
                        style={{ width: 300 }}
                        isOptionEqualToValue={(option, value) => option['id-commune'] === value['id-commune']}
                        // getoptionselected={(option, value) => option['id-commune'] === value['id-commune']}
                        onChange={(event, value) =>
                            handleValidator({
                                target: {
                                    value: value&&value['id-commune'],
                                    name: 'comuna'
                                }
                            },
                                'text')}


                        renderInput={(params) => <CssTextField {...params}
                            name="comuna"
                            value={comuna ? comuna : ''}


                            type="text"
                            label="Comuna"
                            variant='outlined'
                            error={!!valid.comuna}
                            helperText={(valid.comuna ? valid.comuna : '')}
                            size="small"
                            autoComplete='off'

                        />}
                    />

                </Grid>
                <Grid item sm={6} xl={4} md={4} xs={12} >

                    <Autocomplete
                        id="combo-box-demo"
                        loading={crews.length === 0}
                        autoSelect={true}
                        options={crews}
                        getOptionLabel={(option) => `Cuadrilla ${option['id-crew']}`}
                        style={{ width: 300 }}
                        isOptionEqualToValue={(option, value) => option['id-crew'] === value['id-crew']}

                        // getoptionselected=
                        onChange={(event, value) =>
                            handleValidator({
                                target: {
                                    value: value&&value['id-crew'],
                                    name: 'crew'
                                }
                            },
                                'text')}


                        renderInput={(params) => <CssTextField {...params}
                            name="crew"
                            value={idcrew?idcrew:''}


                            type="text"
                            label="Cuadrilla"
                            variant='outlined'
                            error={!!valid.idcrew}
                            helperText={(valid.idcrew ? valid.idcrew : '')}
                            size="small"
                            autoComplete='off'

                        />}
                    />

                </Grid>

                <Grid item sm={6} xl={4} md={4} xs={12} >
                    <CssTextField

                        name="borndate"
                        defaultValue={borndate}
                        onChange={(e) => handleValidator(e, 'text')}
                        placeholder="1238853"
                        type="date"
                        label="Fecha de nac."
                        variant='outlined'
                        error={!!valid.borndate}
                        helperText={(valid.borndate ? valid.borndate : '')}

                        size="small"
                        autoComplete='off'
                        InputLabelProps={{
                            shrink:true
                        }}

                    />
                </Grid>
              

                <Grid item sm={6} xl={4} md={4} xs={12} >
                    <CssTextField

                        name="title"
                        defaultValue={title}
                        onChange={(e) => handleValidator(e, 'text')}
                        placeholder="Ingeniero de redes"
                        type="text"
                        label="Titulo"
                        variant='outlined'
                        error={!!valid.title}
                        helperText={(valid.title ? valid.title : '')}

                        size="small"
                        autoComplete='off'

                    />
                </Grid>
             

            </Grid>
            <Grid item container
                direction="row"
                justify="center"
                alignItems="center">


                <button className="auth__button" type="submit" disabled={!isFormValid()} >
                    Registrar
            </button>


            </Grid>

        </form>
    )
}
