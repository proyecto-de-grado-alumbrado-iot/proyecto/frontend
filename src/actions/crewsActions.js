// dashboard/crewXcommune

import Swal from "sweetalert2";
import { crewsTypes } from "../types/crewTypes";
import { httpToken } from "../utils/fetch";
import { finishLoading, startLoading } from "./uiActions";


export const startGetCrews = (commune) => {
    return async (dispatch) => {
  
      
      try {
      dispatch(startLoading() )

          const body = commune ? {commune}:{}
        const resp = await httpToken(`dashboard/crewXcommune`,body, 'POST');
        const data = await resp.json();
      dispatch(finishLoading() )

        if (data.body) {
          dispatch(loadCrew(data.body));
        } else {
  
          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
  
        }
      } catch (e) {
      dispatch(finishLoading() )

        Swal.fire('Error', `${e}`, 'error');
  
      }
  
    }

};

export const startGetCrewInfo = (idCrew) => {
    return async (dispatch) => {
  
      
      try {

          const body=idCrew ?{crew:idCrew}:{}

        const resp = await httpToken(`dashboard/pendingXcrew`,body, 'POST');
        const data = await resp.json();

        if (data.ok) {

          dispatch(loadCrewInfo(data.body));
        } else {
  
          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
  
        }
      } catch (e) {

        Swal.fire('Error', `${e}`, 'error');
  
      }
  
    }

};

export const startFixFailure = (idFailure) => {
  return async (dispatch) => {

    
    try {

      Swal.fire({
        title: 'Arreglar fallo',
        text: "¿Estas seguro que quieres marcar el fallo como arreglado?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Arreglar!',
        cancelButtonText:'Cancelar'
      }).then(async(result) => {



    
      if (result.isConfirmed) {
        dispatch(startLoading() )

        const resp = await httpToken(`failure/${idFailure}`);
        const data = await resp.json();
        dispatch(finishLoading() )
  
        if (data.ok) {
          // console.log(data)
          // dispatch(loadCrewInfo(data.body));
          Swal.fire(
            'Confirmado',
            'El fallo ha sido arreglado',
            'success'
          )
        } else {
  
          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
  
        }
  
       
      }
    })
    } catch (e) {
      dispatch(finishLoading() )

      Swal.fire('Error', `${e}`, 'error');

    }

  }

};
const loadCrew = (crews)=>({
    type: crewsTypes.loadCrews,
    payload: crews
  
  })

  const loadCrewInfo = (crewInfo)=>({
    type: crewsTypes.loadFailsAndEmployees,
    payload: crewInfo
  
  })
  