
import { LinearProgress } from '@material-ui/core';
import React from 'react';
import loader from '../../assets/1540.jpg';

export const LoadingPage = () => {
    console.log(loader);
    return (
        <>
        <LinearProgress />

        <div className="home__waiting">
            
            <h1 className="animate__animated animate__fadeIn animate__infinite	infinite">Espere un momento...</h1>

            
        </div>
        </>
    )
}
