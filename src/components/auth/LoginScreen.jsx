
import { Grid, InputAdornment } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { startLogin } from '../../actions/authActions';
import { useForm } from '../../hooks/useForm';
import { CssTextField } from '../ui/InputOutlined';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import validator from 'validator'




// render


export const LoginScreen = ({ history }) => {
    const [valid, setValidator] = useState({
        email: '',
        password: ''
    })
    const dispatch = useDispatch();

    const [formValues, handleInputChange, reset] = useForm({
        email: "",
        password: ""

    })

   
    const { email, password } = formValues;

    const handleValidator = (e, type) => {
        handleInputChange(e);
        const value = e.target.value;
        switch (type) {
            case "password":
                if (value.length < 6) {
                    setValidator({
                        ...valid,
                        password: '* La contraseña debe tener 6 caracteres mínimo'
                    })
                } else {
                    setValidator({
                        ...valid,
                        password: null
                    })
                }
                break;
            case "email":
                if (!validator.isEmail(value)) {
                    setValidator({
                        ...valid,
                        email: '* El email no es valido'
                    })

                } else {
                    setValidator({
                        ...valid,
                        email: null
                    })
                }
                break;
            default:
                if(value.length===0)
                setValidator({
                    ...valid,
                    [e.target.name]: '* Campo obligatorio'

                })
                break;
        }
    }

    const handleSubmit = (e) => {


        e.preventDefault();
        if (isFormValid()) {

            dispatch(startLogin(email, password));
        }
    }

    const isFormValid = () => {


        if (!valid.email && !valid.password && email && password) {
            return true
        } else {
            return false
        }

    }
    return (
        <div className="auth__login-box">

            <h1>Login</h1><br />
            <p>Ingresa tu usuario y contraseña para acceder al sistema de control del alumbrado publico</p>
            <br />
            <form key="formLogin" onSubmit={handleSubmit}>
                <Grid container direction={"column"} spacing={2}>
                    <Grid item>
                        <span className='auth__label'>Usuario</span><br />
                        <CssTextField
                            key="email123"
                         
                            name="email"
                            value={email}
                            onChange={(e) => handleValidator(e, 'email')}
                            placeholder="example@mail.com"
                            type="text"
                            label=""
                            variant='outlined'
                            error={!!valid.email}
                            helperText={(valid.email ? valid.email : '')}

                            size="small"
                            autoComplete='off'
                            InputProps={{
                                
                                endAdornment: (
                                    <InputAdornment position="start"> {< AlternateEmailIcon />
                                    } </InputAdornment>
                                )
                            }}
                        />
                        <br />
                    </Grid>
                    <Grid item>

                        <br />
                        <span className='auth__label'>Contraseña</span><br />

                        <CssTextField
                            key="password123"
                            id="password123"
                            name="password"
                            value={password}
                            onChange={(e) => handleValidator(e, 'password')}

                            placeholder="*******"
                            m={5}

                            type="password"
                            label=""
                            variant='outlined'
                            error={!!valid.password}
                            helperText={(valid.password ? valid.password : '')}
                            size="small"
                            autoComplete='off'
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="start"> {< VpnKeyIcon />
                                    } </InputAdornment>
                                )
                            }}
                        />
                        <br />
                    </Grid>
                    <Grid item>

                        <br />

                        <button className="auth__button" type="submit" disabled={!isFormValid()} >
                            Ingresar
            </button><br />
                        <Link className="link" to="/auth/register">

                            <div className="auth__link-register">No tienes una cuenta <span>¿Crear una cuenta?</span></div >
                        </Link>
                    </Grid>
                </Grid>

            </form>

        </div>
    )
}
