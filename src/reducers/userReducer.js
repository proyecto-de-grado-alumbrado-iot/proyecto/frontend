import { userType } from "../types/userTypes";

const initialState = {
    employeeCreated: false,
    userCreated: null,
    users:[]
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case userType.userCreated:

            return {
                ...state,
                userCreated: action.payload
            }
        case userType.employeeCreated:

            return {
                ...state,
                employeeCreated: action.payload
            }

            case userType.loadUsers:

                return {
                    ...state,
                    users: action.payload
                }
        case userType.resetUser:

            return initialState
            
     
        default:
            return state;
    }
}