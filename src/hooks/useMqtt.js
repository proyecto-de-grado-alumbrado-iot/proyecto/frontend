import { useEffect, useState } from 'react';

const mqtt = require("mqtt");


const clientId = "alumbradoMar_" + Math.random().toString(16).substr(2, 8);
const host = "wss://alumbradobga.ga:8093/mqtt";
//No funciona con 8094 porque esta corriendo localmente
const options = {
    port:36987,
    keepalive: 60,
    clientId: clientId,
    username: "web_client",
    password: "private",
    protocolId: "MQTT",
    protocolVersion: 4,
    clean: true,
    reconnectPeriod: 1000,
    connectTimeout: 3 * 1000,
    will: {
        topic: "WillMsg",
        payload: "Connection Closed abnormally..!",
        qos: 0,
        retain: false,
    },
};
export const useMqtt = ( ) => {
    const [client, setClient] = useState(null);
    const [connectStatus, setConnectStatus] = useState('Connect');
    const [payload, setPayload] = useState(null);
    const [isSub, setIsSub] = useState(false);
    const mqttConnect = () => {
          setConnectStatus('Connecting');
          setClient(mqtt.connect(host, options));
        };
        useEffect(() => {
          if (client) {
            console.log(client)
            client.on('connect', () => {
              setConnectStatus('Connected');
            });
            client.on('error', (err) => {
              console.error('Connection error: ', err);
              client.end();
            });
            client.on('reconnect', () => {
              setConnectStatus('Reconnecting');
            });
            client.on('message', (topic, message) => {
              const payload = { topic, message: message.toString() };
              setPayload(payload);
            });
          }
        }, [client]);
        
        const mqttSub = (subscription) => {
            if (client) {
              const { topic, qos } = subscription;
              client.subscribe(topic, { qos }, (error) => {
                if (error) {
                  console.log('Subscribe to topics error', error)
                  return
                }
                setIsSub(true)
              });
            }
          };

          const mqttUnSub = (subscription) => {
            if (client) {
              const { topic } = subscription;
              client.unsubscribe(topic, error => {
                if (error) {
                  console.log('Unsubscribe error', error)
                  return
                }
                setIsSub(false);
              });
            }
          };

          const mqttDisconnect = () => {
            if (client) {
              client.end(() => {
                setConnectStatus('Connect');
              });
            }
          }
    return [ connectStatus, payload, isSub,mqttConnect,mqttDisconnect, mqttUnSub, mqttSub ];

}