import React from 'react'
import MenuIcon from '@material-ui/icons/Menu';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { hideSidebar, showSidebar } from '../../actions/uiActions';
export const AppBar = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const { hide } = useSelector(state => state.ui);

    const handleTitle = () => {
        switch (history.location.pathname) {
            case '/devices':

                return 'Dispositivos';

            case '/dashboard':

                return 'Dashboard';
            case '/employees':

                return 'Cuadrillas y empleados';

            default:
                return 'Dashboard';
        }
    };
    const handleClick = () => {
        if(hide){
            dispatch(showSidebar());
        }else{
            dispatch(hideSidebar());

        }
        
    }
    return (
        <div className="home__appbar">
            <MenuIcon className="pointer" onClick={handleClick} />
            <h2>{`${handleTitle()}`}</h2>

        </div>
    )
}
