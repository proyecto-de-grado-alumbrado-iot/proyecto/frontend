import { authType } from "../types/authTypes";
import { globalType } from "../types/uiTypes";

export const setError = (e) =>({
    type: authType.uiSetError,
    payload: e
});

export const removeError = () => ({
    type: authType.uiRemoveError,
})

export const hideSidebar = () => ({
    type: globalType.uiHideSidebar,
    
})

export const showSidebar = () => ({
    type: globalType.uiShowSidebar,
    
})

export const startLoading = () => ({
    type: globalType.uiStartLoading,
    
})

export const finishLoading = () => ({
    type: globalType.uiFinishLoading,
    
})
