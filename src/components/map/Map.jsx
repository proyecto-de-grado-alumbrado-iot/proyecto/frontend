import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import L from "leaflet"
import streetLight from '../../styles/assets/icons/streetLight.svg';
import streetLightError from '../../styles/assets/icons/streetLightError.svg';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {  addValuesToDevices, getIdDevice } from '../../actions/devicesActions';

export const Map = () => {
  const { devicesList } = useSelector(state => state.devices);
  const devices = devicesList? devicesList : []

  let latSum=0;
  let lonSum=0;

  devices?.forEach( item =>{
    latSum += item["lat-device"];
    lonSum += item["lon-device"];

  }) 
console.log(latSum, lonSum);
  const latProm = (devices?.length>0)?latSum/devices?.length: 7.113498327324501;
  const lonProm = (devices?.length>0)?lonSum/devices?.length:-73.12988878034517;

 
const dispatch = useDispatch()

  const handleClick = (e) =>{
   dispatch( getIdDevice(e.target.options.index) );
   dispatch(addValuesToDevices(  {id:e.target.options.index,current:'Off', voltage:'Off', power:'Off'}));
  

  }

  const [coords, setcoords] = useState([7.1128169665783085, -73.12920213483166])
  useEffect(() => {
    setcoords([latProm,lonProm])
    
  }, [latProm,lonProm])


let streetLightIcon = L.icon({
  iconUrl: streetLight,
  iconRetinaUrl: streetLight,
  iconAnchor: [5, 55],
  popupAnchor: [10, -44],
  iconSize: [25, 55],
 
});

let streetLightErrorIcon = L.icon({
  iconUrl: streetLightError,
  iconRetinaUrl: streetLight,
  iconAnchor: [5, 55],
  popupAnchor: [10, -44],
  iconSize: [25, 55],
 
});


  return (
    <MapContainer center={coords} zoom={15} minZoom={10} scrollWheelZoom={true} >
     
    <TileLayer
    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        // attribution='&copy; OpenStreetMap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        // url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
      />

 
      {
        devices?.map((item) => {
          return (<Marker
          riseOnHover={true}
          eventHandlers={{
            click: handleClick
          }}
          index={item['id-device']}
            icon={item.working? streetLightIcon: streetLightErrorIcon}
          key={item['id-device']} position={[item['lat-device'], item['lon-device']]}>
            <Popup>
              <h3>Dispositivo {item['id-device']}</h3>
              <p>{item['name-device']}</p>
              <p>COMUNA: {item['commune']}</p>
              <p>DIRECCIÓN: {item['address']}</p>
            </Popup>
          </Marker>)
        })
      }

{/* <SetViewOnClick coords={coords}/> */}
    </MapContainer>

  )
};

// function SetViewOnClick({ coords }) {
//   const map = useMap();
//   map.setView(coords, map.getZoom());

//   return null;
// }