import React from 'react';
import { createTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { EmployeeForm } from './EmployeeForm';
import { UserForm } from './UserForm';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },

  active: {
    '& $line': {
      borderColor: '#8f5bd9',
    },
  },
  completed: {
    '& $line': {
      borderColor: '#8f5bd9',
    },
  },
  line: {
    borderColor: '#8f5bd9',
    borderTopWidth: 3,
    borderRadius: 1,
  },
}));

function getSteps() {
  return ['Crear usuario', 'Añadir empleado'];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return <UserForm />;

    case 1:
      return <EmployeeForm />;

   
    default:
      return 'Unknown step';
  }
}



export  const UserStepper=()=> {

    const theme = createTheme({
        palette: {
          primary: {
            light: '#4c3073',
            main: '#8f5bd9',
            dark: '#2a1b40',
            contrastText: '#fff',
          },
          secondary: {
            light: '#ff8d73',
            main: '#f2522e',
            dark: '#ee3d16',
            contrastText: '#fff',
          },
        },
      });
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [skipped, setSkipped] = React.useState(new Set());
  const steps = getSteps();

  const isStepOptional = (step) => {
    return step === 2;
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <ThemeProvider theme={theme}>
    <div className={classes.root}>
      <Stepper activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps = {};
          const labelProps = {};
          if (isStepOptional(index)) {
            labelProps.optional = <Typography variant="caption">Optional</Typography>;
          }
          if (isStepSkipped(index)) {
            stepProps.completed = false;
          }
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>
      <div className="user__button">
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>
              Empleado creado - has terminado
            </Typography>
            <Button onClick={handleReset} className={classes.button}>
              Borrar
            </Button>
          </div>
        ) : (
          <div>
           {getStepContent(activeStep)}
            <div>
              <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                Atras
              </Button>
              {isStepOptional(activeStep) && (
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={handleSkip}
                  className={classes.button}
                >
                  Skip
                </Button>
              )}

              <Button
                variant="contained"
                color="secondary"
                onClick={handleNext}
                className={classes.button}
              >
                {activeStep === steps.length - 1 ? 'Terminar' : 'Siguiente'}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
    </ThemeProvider> 
  );
}